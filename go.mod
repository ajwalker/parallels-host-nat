module gitlab.com/ajwalker/parallels-host-nat

go 1.18

require (
	github.com/insomniacslk/dhcp v0.0.0-20220405050111-12fbdcb11b41
	golang.org/x/net v0.0.0-20220412020605-290c469a71a5
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad
)

require github.com/u-root/uio v0.0.0-20210528114334-82958018845c // indirect
