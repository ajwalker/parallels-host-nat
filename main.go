package main

import (
	"context"
	"flag"
	"os"
	"os/signal"

	"gitlab.com/ajwalker/parallels-host-nat/internal/network"
)

func main() {
	var (
		num        int
		dir        string
		configPath string
		install    bool
	)

	flag.IntVar(&num, "n", 4, "number of isolation networks to create")
	flag.StringVar(&dir, "dir", "/tmp/parallels.leases", "directory to store dhcp leases")
	flag.StringVar(&configPath, "config", "/etc/parallels-host-nat/config.json", "config path")
	flag.BoolVar(&install, "install", false, "install isolation networks and write config")
	flag.Parse()

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	var err error
	if install {
		err = network.Install(ctx, configPath, num)
	} else {
		err = network.Serve(ctx, dir, configPath)
	}

	if ctx.Err() != nil {
		return
	}
	if err != nil {
		panic(err)
	}
}
