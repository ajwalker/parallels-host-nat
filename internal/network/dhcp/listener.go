package dhcp

import (
	"syscall"

	"golang.org/x/sys/unix"
)

func listen() (int, error) {
	var fd int

	// isSocket detects if fd=0 is a socket, and if so, this typically means
	// that we're a launchd service executed with inetd compatibility.
	if isSocket() {
		configure(0)
		return 0, nil
	}

	fd, err := unix.Socket(unix.AF_INET, unix.SOCK_DGRAM, unix.IPPROTO_UDP)
	if err != nil {
		return 0, err
	}
	configure(fd)

	// Bind to the port.
	saddr := unix.SockaddrInet4{Port: 67}
	if err := unix.Bind(fd, &saddr); err != nil {
		return 0, err
	}

	return fd, nil
}

func configure(fd int) {
	unix.SetsockoptInt(fd, unix.IPPROTO_IP, unix.IP_RECVIF, 1)
	unix.SetsockoptInt(fd, unix.SOL_SOCKET, unix.SO_BROADCAST, 1)
	unix.SetsockoptInt(fd, unix.SOL_SOCKET, unix.SO_REUSEADDR, 1)
	unix.SetsockoptInt(fd, unix.IPPROTO_IP, unix.IP_RECVDSTADDR, 1)
}

func isSocket() bool {
	var stat syscall.Stat_t
	if err := syscall.Fstat(0, &stat); err != nil {
		return false
	}

	switch stat.Mode & syscall.S_IFMT {
	case syscall.S_IFCHR, syscall.S_IFREG, syscall.S_IFLNK, syscall.S_IFDIR, syscall.S_IFBLK:
		return false
	}
	return true
}
