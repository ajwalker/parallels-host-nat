package dhcp

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

var ErrUnableToAllocate = errors.New("unable to allocate address")

type Pool struct {
	dir string
	mu  sync.Mutex
}

func NewPool(dir string) (*Pool, error) {
	err := os.MkdirAll(dir, 0777)
	if err != nil {
		return nil, fmt.Errorf("creating pool dir: %w", err)
	}

	return &Pool{dir: dir}, nil
}

func (p *Pool) get(mac string) net.IP {
	b, err := ioutil.ReadFile(filepath.Join(p.dir, mac))
	if err != nil {
		return nil
	}

	return net.IP(b)
}

func (p *Pool) set(mac string, ip net.IP) error {
	f, err := os.OpenFile(filepath.Join(p.dir, mac+".tmp"), os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		return fmt.Errorf("creating lease temporary file for %q: %w", mac, err)
	}

	if _, err := f.Write(ip.To4()); err != nil {
		return fmt.Errorf("writing lease temporary file for %q: %w", mac, err)
	}

	if err := f.Close(); err != nil {
		return fmt.Errorf("closing lease temporary file for %q: %w", mac, err)
	}

	os.Chmod(f.Name(), 0666)
	if err := os.Rename(f.Name(), filepath.Join(p.dir, mac)); err != nil {
		return fmt.Errorf("creating lease file for %q: %w", mac, err)
	}

	return nil
}

func (p *Pool) has(ip net.IP) bool {
	err := filepath.WalkDir(p.dir, func(path string, d fs.DirEntry, err error) error {
		if strings.HasSuffix(d.Name(), ".tmp") {
			return nil
		}
		if d.IsDir() {
			return nil
		}

		if p.get(d.Name()).Equal(ip) {
			return io.EOF
		}
		return nil
	})

	return err == io.EOF
}

func (p *Pool) Allocate(mac string, start, end net.IP) (net.IP, error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	start = start.To4()
	end = end.To4()

	if len(start) < 4 || len(end) < 4 {
		return nil, ErrUnableToAllocate
	}

	lease := p.get(mac)
	if lease != nil {
		ip := binary.BigEndian.Uint32(lease)
		if ip > binary.BigEndian.Uint32(start.To4()) && ip <= binary.BigEndian.Uint32(end.To4()) {
			return lease, nil
		}
	}

	check := make([]byte, 4)
	for i := binary.BigEndian.Uint32(start) + 1; i <= binary.BigEndian.Uint32(end); i++ {
		binary.BigEndian.PutUint32(check, i)
		if p.has(check) {
			continue
		}

		if err := p.set(mac, check); err != nil {
			return nil, err
		}

		return check, nil
	}

	return nil, ErrUnableToAllocate
}

func Add(ip net.IP, num uint32) net.IP {
	ipv4 := make([]byte, 4)
	binary.BigEndian.PutUint32(ipv4, binary.BigEndian.Uint32(ip.To4())+num)
	return ipv4
}
