package dhcp

import (
	"errors"
	"net"
	"testing"
)

func TestPool(t *testing.T) {
	dir := t.TempDir()

	p, err := NewPool(dir)
	if err != nil {
		t.Fatal(err)
	}

	allocated, err := p.Allocate("3f2fff3f4f6f", net.IPv4(10, 10, 10, 0), net.IPv4(10, 10, 10, 2))
	if err != nil {
		t.Fatal(err)
	}
	if !allocated.Equal(net.IPv4(10, 10, 10, 1)) {
		t.Errorf("expected %s, got %s", net.IPv4(10, 10, 10, 1), allocated)
	}

	leaseIP := p.get("3f2fff3f4f6f")
	if !leaseIP.Equal(allocated) {
		t.Errorf("expected %s, got %s", allocated, leaseIP)
	}

	// reopen
	p, err = NewPool(dir)
	if err != nil {
		t.Fatal(err)
	}

	// fetch existing
	leaseIP = p.get("3f2fff3f4f6f")
	if !leaseIP.Equal(allocated) {
		t.Errorf("expected %s, got %s", allocated, leaseIP)
	}

	// allocate another
	allocated, err = p.Allocate("3f2fff3fffff", net.IPv4(10, 10, 10, 0), net.IPv4(10, 10, 10, 2))
	if err != nil {
		t.Fatal(err)
	}

	if !allocated.Equal(net.IPv4(10, 10, 10, 2)) {
		t.Errorf("expected %s, got %s", net.IPv4(10, 10, 10, 2), allocated)
	}

	// allocating another should fail
	_, err = p.Allocate("3fffffffffff", net.IPv4(10, 10, 10, 0), net.IPv4(10, 10, 10, 2))
	if !errors.Is(err, ErrUnableToAllocate) {
		t.Errorf("expected %v, got %v", ErrUnableToAllocate, err)
	}
}
