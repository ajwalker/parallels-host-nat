package dhcp

import (
	"context"
	"encoding/hex"
	"log"
	"net"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/insomniacslk/dhcp/dhcpv4"
	"golang.org/x/net/ipv4"
)

type Config struct {
	Gateway    net.IP
	StartRange net.IP
	EndRange   net.IP
}

type Server struct {
	pool *Pool
	mu   sync.Mutex
	m    map[string]Config
}

func NewServer(pool *Pool) *Server {
	return &Server{pool: pool, m: make(map[string]Config)}
}

func (s *Server) AddConfig(name string, config Config) {
	s.mu.Lock()
	defer s.mu.Unlock()

	log.Printf("adding dhcp config for network %q: %v", name, config)

	s.m[name] = config
}

func getAddr(iface *net.Interface) string {
	addrs, err := iface.Addrs()
	if err != nil {
		return ""
	}

	for _, addr := range addrs {
		if strings.Contains(addr.String(), ":") {
			continue
		}

		ip, _, _ := net.ParseCIDR(addr.String())
		return ip.String()
	}

	return ""
}

func (s *Server) Serve(ctx context.Context) error {
	fd, err := listen()
	if err != nil {
		return err
	}

	p := make([]byte, 4096)
	oob := make([]byte, 4096)

	// close connections when context is canceled
	go func() {
		<-ctx.Done()
		syscall.Close(fd)
	}()

	var cm ipv4.ControlMessage
	for {
		n, oobn, _, from, err := syscall.Recvmsg(fd, p, oob, 0)
		if err != nil {
			return err
		}
		if oobn > 0 {
			if err := cm.Parse(oob[:oobn]); err != nil {
				return err
			}
		}

		peer, ok := from.(*syscall.SockaddrInet4)
		if !ok {
			continue
		}

		if net.IP(peer.Addr[:]).To4().Equal(net.IPv4zero) {
			peer.Addr[0] = 0xff
			peer.Addr[1] = 0xff
			peer.Addr[2] = 0xff
			peer.Addr[3] = 0xff
		}

		m, err := dhcpv4.FromBytes(p[:n])
		if err != nil {
			continue
		}

		if m.OpCode != dhcpv4.OpcodeBootRequest {
			continue
		}

		iface, err := net.InterfaceByIndex(cm.IfIndex)
		if err != nil {
			continue
		}

		s.mu.Lock()
		config, ok := s.m[getAddr(iface)]
		s.mu.Unlock()
		if !ok {
			continue
		}

		reply, err := dhcpv4.NewReplyFromRequest(m)
		if err != nil {
			continue
		}

		reply.YourIPAddr, err = s.pool.Allocate(hex.EncodeToString(m.ClientHWAddr), config.StartRange, config.EndRange)
		if err != nil {
			continue
		}
		reply.ServerIPAddr = config.Gateway

		reply.UpdateOption(dhcpv4.OptIPAddressLeaseTime(m.IPAddressLeaseTime(24 * time.Hour)))
		reply.UpdateOption(dhcpv4.OptSubnetMask(net.IPv4Mask(255, 255, 255, 0)))
		reply.UpdateOption(dhcpv4.OptRouter(config.Gateway))
		reply.UpdateOption(dhcpv4.OptServerIdentifier(reply.ServerIPAddr))
		reply.UpdateOption(dhcpv4.OptDNS(net.IPv4(1, 1, 1, 1), net.IPv4(1, 0, 0, 1)))

		switch m.MessageType() {
		case dhcpv4.MessageTypeDiscover:
			reply.UpdateOption(dhcpv4.OptMessageType(dhcpv4.MessageTypeOffer))

		case dhcpv4.MessageTypeRequest:
			if reply.YourIPAddr.Equal(dhcpv4.GetIP(dhcpv4.OptionRequestedIPAddress, m.Options)) {
				reply.UpdateOption(dhcpv4.OptMessageType(dhcpv4.MessageTypeAck))
			} else {
				reply.UpdateOption(dhcpv4.OptMessageType(dhcpv4.MessageTypeNak))
			}
		}

		log.Println("req", m)
		log.Println("reply", reply)

		cm.Src = nil
		cm.Dst = nil
		cm.TTL = 0

		syscall.Sendmsg(fd, reply.ToBytes(), cm.Marshal(), peer, 0)
	}
}
