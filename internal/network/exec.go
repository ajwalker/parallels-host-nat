package network

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"strings"
)

func run(ctx context.Context, stdin string, commands ...string) (string, error) {
	var stdout strings.Builder
	var stderr strings.Builder

	if stdin == "" {
		log.Println("running:", strings.Join(commands, " "))
	} else {
		log.Println("running:", strings.Join(commands, " "), fmt.Sprintf("[%q]", stdin))
	}

	cmd := exec.CommandContext(ctx, commands[0], commands[1:]...)
	cmd.Stdin = strings.NewReader(stdin)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	var errExit *exec.ExitError
	if errors.As(err, &errExit) {
		return stdout.String(), fmt.Errorf("%s: %w (%s)", strings.Join(commands, " "), err, stderr.String())
	}
	if err != nil {
		return stdout.String(), fmt.Errorf("%s: %w", commands[0], err)
	}

	return stdout.String(), nil
}
