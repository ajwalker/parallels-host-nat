package network

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/ajwalker/parallels-host-nat/internal/network/dhcp"
	"golang.org/x/net/route"
)

type Network struct {
	Name      string
	Address   string
	Interface string
}

func Install(ctx context.Context, configPath string, num int) error {
	os.MkdirAll(filepath.Dir(configPath), 0777)

	for i := 0; i < num; i++ {
		run(ctx, "", "prlsrvctl", "net", "add", fmt.Sprintf("isolation-%d", i))
	}

	var addresses []string
	for i := 0; i < num; i++ {
		name := fmt.Sprintf("isolation-%d", i)

		_, err := run(ctx, "", "prlsrvctl", "net", "set", name, "--dhcp-server=off", "--dhcp6-server=off", "--connect-host-to-net=on")
		if err != nil {
			return fmt.Errorf("updating network settings: %w", err)
		}

		raw, err := run(ctx, "", "prlsrvctl", "net", "info", name, "-j")
		if err != nil {
			return fmt.Errorf("fetching network info: %w", err)
		}

		var info struct {
			Adapter struct {
				Address string `json:"IPv4 address"`
			} `json:"Parallels adapter"`
		}

		if err := json.Unmarshal([]byte(raw), &info); err != nil {
			return err
		}

		addresses = append(addresses, info.Adapter.Address)
	}

	data, _ := json.MarshalIndent(addresses, "", " ")
	return os.WriteFile(configPath, data, 0777)
}

func Serve(ctx context.Context, dir string, configPath string) error {
	cfg, err := os.ReadFile(configPath)
	if err != nil {
		return fmt.Errorf("reading config: %w", err)
	}

	var addresses []string
	if err := json.Unmarshal(cfg, &addresses); err != nil {
		return fmt.Errorf("unmarshaling config: %w", err)
	}

	log.Printf("serving using %v, %d networks\n", dir, len(addresses))
	pool, err := dhcp.NewPool(dir)
	if err != nil {
		return err
	}

	srv := dhcp.NewServer(pool)

	if _, err := run(ctx, "", "sysctl", "-w", "net.inet.ip.forwarding=1"); err != nil {
		return fmt.Errorf("enabling ip forwarding: %w", err)
	}

	var iface string
	for i := 0; i < 30; i++ {
		iface, _ = getDefaultGatewayInterface()
		if iface == "" {
			time.Sleep(2 * time.Second)
			continue
		}
		break
	}

	if iface == "" {
		return fmt.Errorf("unable to get default gateway interface")
	}

	for i := 0; i < len(addresses); i++ {
		go func(addresses []string, idx int) {
			name := fmt.Sprintf("isolation-%d", idx)

			err := setupBridge(ctx, srv, name, iface, addresses[idx])
			if err != nil {
				log.Printf("setting up bridge for %s: %v", name, err)
			}
		}(addresses, i)
	}

	return srv.Serve(ctx)
}

func setupBridge(ctx context.Context, srv *dhcp.Server, network, iface, ip string) error {
	rule := fmt.Sprintf("nat on %s from %s/24 to any -> (%s)\n", iface, ip, iface)
	rule += fmt.Sprintf("pass in quick from %s/24\n", ip)

	_, err := run(ctx, rule, "pfctl", "-a", fmt.Sprintf("com.apple/%s", network), "-f", "-")
	if err != nil {
		return fmt.Errorf("configuring network (%q) firewall: %w", network, err)
	}

	start := net.ParseIP(ip)
	srv.AddConfig(ip, dhcp.Config{
		Gateway:    start,
		StartRange: dhcp.Add(start, 1),
		EndRange:   dhcp.Add(start, 20),
	})

	return nil
}

func getDefaultGatewayInterface() (string, net.IP) {
	rib, _ := route.FetchRIB(0, route.RIBTypeRoute, 0)
	messages, err := route.ParseRIB(route.RIBTypeRoute, rib)
	if err != nil {
		return "", nil
	}

	for _, message := range messages {
		msg := message.(*route.RouteMessage)
		addresses := msg.Addrs

		if len(addresses) < 2 {
			continue
		}

		dst, ok := msg.Addrs[0].(*route.Inet4Addr)
		if !ok {
			continue
		}

		if dst.IP != [4]byte{0, 0, 0, 0} {
			continue
		}

		iface, err := net.InterfaceByIndex(msg.Index)
		if err != nil {
			break
		}

		addrs, err := iface.Addrs()
		if len(addrs) == 0 || err != nil {
			break
		}

		for _, addr := range addrs {
			if strings.Contains(addr.String(), ":") {
				continue
			}

			ip, _, _ := net.ParseCIDR(addr.String())
			return iface.Name, ip
		}

		break
	}

	return "", nil
}
