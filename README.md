# parallels-host-nat

`parallels-host-nat` provides a daemon to setup isolated VM networks with
Internet connectivity.

Parallels offers two networking modes:

- Shared: Allowing VMs to contact one another and the Internet.
- Host-Only: Allowing VMs using the same network to contact each other.

Unfortunately, there is no built-in networking mode that allows VMs to be
isolated from one another whilst still being able to access the Internet.

This daemon provides this functionality by creating multiple Host-Only networks,
so that each VM has its own subnet (VMs are therefore isolated from each other)
but also configures NAT so that each VM is able to access the Internet.

Parallels' built in DHCP for a Host-Only network doesn't setup the correct
gateway and routing for Internet access, so this daemon also serves DHCP
requests from the interfaces Parallels creates.

## usage

On a MacOS system with Parallels installed:

```
# setup 4 isolation networks and write config
# a parallels license-key is required for setup
sudo ./parallels-host-nat -install -n 4

# serve (this can also be run as a service, example below)
# a parallels license-key is not required for adding NAT rules/serving dhcp requests
sudo ./parallels-host-nat -dir ./leases
```

This will store DHCP leases in the directory provided. It is intended for these
leases to be inspected and removed by external processes.

### as a service

```shell
sudo tee /Library/LaunchDaemons/parallels-host-nat.plist <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>Label</key>
        <string>parallels-host-nat</string>

        <key>Program</key>
        <string>/usr/local/bin/parallels-host-nat</string>

        <key>EnvironmentVariables</key>
        <dict>
            <key>PATH</key>
            <string>/sbin:/usr/bin:/usr/sbin:/usr/local/bin</string>
        </dict>

        <key>StandardOutPath</key>
        <string>/var/log/parallels-host-nat.log</string>

        <key>StandardErrorPath</key>
        <string>/var/log/parallels-host-nat.log</string>

        <key>Sockets</key>
        <dict>
            <key>Listeners</key>
            <dict>
                <key>SockServiceName</key>
                <string>bootps</string>
                <key>SockType</key>
                <string>dgram</string>
                <key>SockFamily</key>
                <string>IPv4</string>
                <key>ReceivePacketInfo</key>
                <true />
            </dict>
        </dict>

        <key>inetdCompatibility</key>
        <dict>
            <key>Wait</key>
            <true />
        </dict>

        <key>KeepAlive</key>
        <true />

        <key>RunAtLoad</key>
        <true />
    </dict>
</plist>
EOF

sudo launchctl load /Library/LaunchDaemons/parallels-host-nat.plist
```

